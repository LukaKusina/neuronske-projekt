import os
import timeit
from shutil import copyfile


image_directory = "/Users/lukakusina/Faks/Neuronske/projekt/data/utkface_aligned_cropped/UTKFace"

gender_classification_directory = "/Users/lukakusina/Faks/Neuronske/projekt/generated/gender_classifications"
age_classification_directory = "/Users/lukakusina/Faks/Neuronske/projekt/generated/age_classifications"

age_labels = ["0-7", "8-15", "16-25", "26-35",
              "36-45", "46-60", "61-75", "76-90", "91-116"]  # calculates age from lower -1 < age <= upper


def ageLabel(age):
    for a in age_labels:
        constraints = a.split("-")
        if age > int(constraints[0])-1 and age <= int(constraints[1]):
            return a
    return "unclassified"


if __name__ == "__main__":

    start = timeit.default_timer()

    male_dir = gender_classification_directory + "/0"
    female_dir = gender_classification_directory + "/1"

    if not os.path.exists(male_dir):
        os.makedirs(male_dir)

    if not os.path.exists(female_dir):
        os.makedirs(female_dir)

    counter = 0
    age_distribution = {}
    for img in os.scandir(image_directory):

        if img.path.endswith(".jpg") and img.is_file():
            data = img.name.split("_")
            age = int(data[0])
            gender = data[1]
            copyfile(img.path, os.path.join(
                male_dir if gender == "0" else female_dir,
                img.name))

            age_group = ageLabel(age)
            age_dir_path = os.path.join(
                age_classification_directory, age_group)
            if not os.path.exists(age_dir_path):
                os.makedirs(age_dir_path)
            copyfile(img.path, os.path.join(age_dir_path, img.name))

            if age_group not in age_distribution:
                age_distribution[age_group] = 0
            age_distribution[age_group] += 1

    for a in sorted(age_distribution.keys()):
        print("Age group = %s count : %d" % (a, age_distribution[a]))

    # Your statements here

    stop = timeit.default_timer()
    print('Time: ', stop - start)
